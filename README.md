Generate secure mail adresses
======================

What's this?
----------------------
This small tool is intended to generate mail adresses, which can be denied in Postfix, whenever they appear in a spam list.

When registering I choose an adress, which fits the domain of the site (e.g. adobe.com-1ac41985759a@strong.example.com). E-Mails to @strong.example.com will be forwarded to the main adress via catch-all. This tool helps you generating that adresses.

If an adress will be stolen in a hacker accident, I simply enter them in my receiver blacklist in Postfix. Then I go to the service and change the registered mail adress to a new adress (adobe.com-2a69a8df04bb@strong.example.com). Then adobe.com-3a7fafb6dd8b@strong.example.com would be the next and so on...

How does it work?
----------------------
The front part of the mail adress is hashed with a secret. Then this hash is cut to 10 characters and the adress, which's parts are $domain-$version$hash\_id$hash\_short@strong.example.com, is shown.  
$domain: The domain you entered  
$version: The version you entered  
$hash\_id: The id of your secret password. Whenever you need to change the password, you can always identify which password you used when you generated the mail adress  
$hash\_short: The hash of the adress, cut to 10 characters  
@strong.example.com: The catch-all adress  

What do you need?
----------------------
You need a catch-all adress like @strong.example.com, which forwards to you main adress.

