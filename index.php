<?php
/* edit here */
$hash_text = "Superstrong Password"; // A secret password, which is used to secure the hash
$hash_id = "a"; // The id of your secret password. Whenever you need to change the password, you can always identify which password you used when you generated the mail adress
$mail_domain = "strong.example.com"; // The domain you have set up as catch-all adress. This adress should forward all mails to your main adress.
?>

<html>
<head>
<title>Generate secure mail adresses</title>
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.0a3/jquery.mobile-1.0a3.min.css" />
<script src="http://code.jquery.com/jquery-1.5.min.js"></script>
<script src="http://code.jquery.com/mobile/1.0a3/jquery.mobile-1.0a3.min.js"></script>
</head>

<body>

<!-- Page -->
<div data-role="page">
<!-- Header -->
	<div data-role="header">Generate secure mail adress</div>
<!-- Content -->
	<div data-role="content">
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
			<div data-role="fieldcontain">
			<label for="name">Domain:</label>
			<input type="text" name="name" id="name" placeholder="Domain"  />
			<p><?php echo "@".$mail_domain; ?></p>
			<div data-role="fieldcontain">
			<label for="rev" class="select">Version:</label>
			<select name="ver" id="ver">
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9">9</option>
				<option value="10">10</option>
			</select>
			<p>if adress has been stolen, add 1 to old version</p>
			<input type="submit" value="Abschicken" />
		</form>


<?php
If(!empty($_POST['name'])){
$hash_hash = sha1($hash_text);
$sha_orig = sha1(mb_strtolower($_POST['name'])."-".$_POST['ver']."-".$hash_hash);
$sha_short = substr($sha_orig,0 ,10 );
echo "<input type=\"text\" value=\"".mb_strtolower($_POST['name'])."-".$_POST['ver'].$hash_id.$sha_short."@".$mail_domain."\"/>";
}

?>
</div>

<!-- Footer -->
    <div data-role="footer"></div>
</div>

